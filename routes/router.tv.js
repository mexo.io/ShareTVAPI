const express = require('express');
const mongoose = require('mongoose');
const TV = require('../model/model.tv');
const router = express.Router();

function buildTV(req) {
    return new TV({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        key: req.body.key,
        img: req.body.img
    });
}

router.get('/', function (req, res) {
    TV.find({}, function (err, users) {
        res.send(users);
    });
});

router.post('/insert', function (req, res) {
    TV.findOne({title: {'$regex': req.body.title, '$options': 'i'}}, function (err, doc) {
        if (doc) {
            return res.status(200).json({
                status: false,
                message: 'Movie already exist',
                tv: doc
            });
        } else {
            buildTV(req).save().then(() => {
                return res.status(201).json({
                    status: true
                });
            }).catch(error => {
                return res.status(500).json({
                    error
                });
            });
        }
    });
});

router.put('/update', function (req, res) {
    TV.findOneAndUpdate({title: req.body.title}, req.body, {upsert: false}, function (err) {
        if (err) return res.send(500, {error: err});
        return res.status(200).json({
            status: true
        });
    });
});

router.post('/search', function (req, res) {
    TV.findOne({title: {'$regex': req.body.title, '$options': 'i'}}, function (err, doc) {
        if (err) return res.send(500, {error: err});
        return res.status(200).json({
            status: true,
            data: doc
        });
    });
});

module.exports = router;
