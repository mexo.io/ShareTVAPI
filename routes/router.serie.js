const express = require('express');
const mongoose = require('mongoose');
const Serie = require('../model/model.serie');
const check = require('../utils/server.check');
const router = express.Router();

function shuffle(array) {
    return array.sort(function () {
        return 0.5 - Math.random()
    });
}

function buildSerie(req) {
    return new Serie({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        img: req.body.img,
        year: req.body.year,
        director: req.body.director,
        synopsis: req.body.synopsis,
	puntuation: req.body.puntuation,
        trailer: req.body.trailer,
        size: req.body.size,
        quality: req.body.quality,
        language: req.body.language,
        uploader: req.body.uploader,
        genre: req.body.genre,
        seasons: check.series_link(req.body.seasons)
    });
}

router.get('/', function (req, res) {
    Serie.find({}, function (err, torrents) {
        res.send(shuffle(torrents));
    });
});

router.post('/search', function (req, res) {
    Serie.find({
        title: {
            '$regex': req.body.title,
            '$options': 'i'
        }
    }, function (err, doc) {
        if (err) return res.send(500, {
            error: err
        });
        return res.status(200).json({
            status: true,
            data: doc
        });
    });
});

router.post('/insert', function (req, res) {
    Serie.findOne({
        title: req.body.title
    }, function (err, doc) {
        if (doc) {
            return res.status(200).json({
                status: false,
                message: 'Serie already exist',
                serie: doc
            });
        } else {
            buildSerie(req).save().then(() => {
                return res.status(201).json({
                    status: true
                });
            }).catch(error => {
                return res.status(500).json({
                    error
                });
            });
        }
    });
});

router.put('/update', function (req, res) {
    req.body.seasons = check.series_link(req.body.seasons);
    Serie.findOneAndUpdate({
        title: req.body.title
    }, req.body, {
        upsert: false
    }, function (err) {
        if (err) return res.send(500, {
            error: err
        });
        return res.status(200).json({
            status: true
        });
    });
});

router.get('/genre/:id', function (req, res) {
    Serie.find({
        genre: {
            $all: [req.params.id]
        }
    }, function (err, torrents) {
        res.send(shuffle(torrents));
    });
});

module.exports = router;
