const mongoose = require('mongoose');

const user = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {type: String, required: true},
    nickname: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    country: {type: String, required: true},
    refreshToken: {type: String, required: false},
    ip: {type: String, required: true},
    premium: {type: Boolean, required: false},
    wallet: {
        seed: {type: String, required: true},
        address: {type: String, required: true},
        privateKey: {type: String, required: true},
        publicKey: {type: String, required: true}
    }
});

module.exports = mongoose.model('User', user);