const mongoose = require('mongoose');

const serie = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String, required: true},
    img: {type: String, required: true},
    year: {type: String, required: true},
    synopsis: {type: String, required: true},
    trailer: {type: String, required: true},
    size: {type: String, required: true},
    quality: {type: String, required: true},
    uploader: {type: String, required: true},
    classification: {type: String, required: false},
    producer: {type: String, required: false},
    created: {type: Date, default: Date.now},
    puntuation: {type: String, required: false},
    writer: {type: String, required: false},
    director: [{type: String}],
    cast: [{type: String}],
    genre: [{type: String}],
    seasons: [
        {
            caps: [{
                openload: {type: Boolean, required: true},
                rapidvideo: {type: Boolean, required: true},
                streamango: {type: Boolean, required: true},
                key: {type: String, required: true},
                synopsis: {type: String, required: false},
                language: {type: String, required: true},
            }]
        }
    ]
});

module.exports = mongoose.model('Serie', serie);
